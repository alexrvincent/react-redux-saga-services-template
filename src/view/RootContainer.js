import React, { Component } from 'react';
import './App.css';

import LoginTypes from '../redux/LoginRedux';
import { connect } from 'react-redux';

class RootContainer extends Component {

    handleClick = () => {
        this.props.loginUser();
    };

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">App Name</h1>
                    <h3>App-Redux-Sagas-Services Template</h3>
                </header>
                <button onClick={this.handleClick}> Login with Office 365</button>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        state: state
    }
};

const mapDispatchToProps = dispatch => {
    return {
        loginUser: () => dispatch(LoginTypes.loginRequest({data: 'hello'}))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);