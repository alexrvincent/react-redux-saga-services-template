import React, { Component } from 'react';

import createStore from '../redux/index'
import RootContainer from './RootContainer'

import { Provider } from 'react-redux';

const store = createStore();

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <RootContainer/>
        </Provider>
    );
  }
}

export default App;
