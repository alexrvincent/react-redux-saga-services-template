export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default {
    getLogin: async () => {
        await sleep(3000);
        return {
            ok: true,
            data: {}
        }
    }
}
