/* index.js
*
* The purpose of this file is to assemble our entire redux architecture by:
* 1) Importing and assembling our all of our app reducers into a single root reducer for the redux
* 2) Importing the custom-assembled root saga
* 3) Wrapping our rootReducer and rootSaga in a custom redux store configuration
*
* */

/* Custom wrapper function to configure the store with enhancers / middleware */
import configureStore from './createStore';

/* All app reducers imported and combined here  */
import { combineReducers } from 'redux';
import { loginReducer } from "./LoginRedux";

/* All sagas imported (after being combined seperately) */
import rootSaga from '../sagas';

export default () => {

    /* Assemble and combine the app reducers into a rootReducer */
    const rootReducer = combineReducers({
        login: loginReducer
    });

    return configureStore(rootReducer, rootSaga);
};