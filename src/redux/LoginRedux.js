import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    loginRequest: ['data'],
    loginSuccess: ['data'],
    loginFailure: null,
    loginClear: null,
});

export const LoginTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    fetching: false,
    data: null,
    error: null,
    client_id: null,
});

/* ------------- State Modifier Functions -------------- */
// None!

/* ------------- Reducers ------------- */

export const request = state => state.merge({ fetching: true });
export const success = (state, { data }) => state.merge({ fetching: false, error: null, data });
export const failure = state => state.merge({ fetching: false, error: true, data: null });
export const clear = state => state.merge({ data: null, error: null });

/* ------------- Hookup Reducers To Types ------------- */

export const loginReducer = createReducer(INITIAL_STATE, {
    [Types.LOGIN_REQUEST]: request,
    [Types.LOGIN_SUCCESS]: success,
    [Types.LOGIN_FAILURE]: failure,
    [Types.LOGIN_CLEAR]: clear,
});

/* ------------- Selectors ------------- */