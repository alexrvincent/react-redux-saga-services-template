import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

// creates the store
export default (rootReducer, rootSaga) => {

    /* ------------- Redux Configuration ------------- */

    const middleware = [];
    //const enhancers = [];
    const composeSetup =
        process.env.NODE_ENV !== "production" &&
        typeof window === "object" &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            : compose;



    /* ------------- Saga Middleware ------------- */
    const sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);

    /* ------------- Assemble Middleware ------------- */

    //enhancers.push(applyMiddleware(...middleware));

    // Create the store
    const store = createStore(rootReducer, composeSetup(applyMiddleware(...middleware)));

    // kick off root saga
    sagaMiddleware.run(rootSaga);

    return store;
};
