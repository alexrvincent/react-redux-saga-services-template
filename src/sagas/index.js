import { takeEvery, all } from 'redux-saga/effects';
//import API from '../services/api';
import FixtureAPI from '../services/fixtureApi';

/* ------------- Types ------------- */

import { LoginTypes } from '../redux/LoginRedux';

/* ------------- Sagas ------------- */

import { getLogin } from './LoginSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
// Note: If you'd like to use fixtures for a specific saga action, replace
//       'api' with 'fixtureApi'
//const api = API.create();
const fixtureApi = FixtureAPI;

/* ------------- Connect Types To Sagas ------------- */

// some sagas only receive an action
// some sagas receive extra parameters in addition to an action
export default function* root() {
    yield all([
        takeEvery(LoginTypes.LOGIN_REQUEST, getLogin, fixtureApi)
    ]);
}