import { call, put } from 'redux-saga/effects';
import LoginActions from '../redux/LoginRedux';

export function * getLogin(api, action) {
    // Make API Call
    const response = yield call(api.getLogin, action.data);
    if (response.ok) {
        console.log(action);
        yield put(LoginActions.loginSuccess({}));
    } else {
        yield put(LoginActions.loginFailure('Error'));
    }
}